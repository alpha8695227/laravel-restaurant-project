<?php

use App\Http\Controllers\FrontendController;
use Illuminate\Support\Facades\Route;


Route::get('/', [FrontendController::class, 'showHomePage']) -> name('page.home');
Route::get('/staff', [FrontendController::class, 'showStaffPage']) -> name('page.staff');
Route::get('/menu', [FrontendController::class, 'showMenuPage']) -> name('page.menu');
Route::get('/news', [FrontendController::class, 'showNewsPage']) -> name('page.news');
Route::get('/gallery', [FrontendController::class, 'showGalleryPage']) -> name('page.gallery');

Route::get('/wel', function(){
    return view('welcome');
});

Route::get('/here',function(){
    echo "route";
});
