<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    //showHomePage
    public function showHomePage(){
      return view('index');
    }
    //showStaffPage
    public function showStaffPage(){
        return view('staff');
      }
      //showMenuPage
    public function showMenuPage(){
        return view('menu');
      }
      //showNewsPage
    public function showNewsPage(){
        return view('news');
      }
}
